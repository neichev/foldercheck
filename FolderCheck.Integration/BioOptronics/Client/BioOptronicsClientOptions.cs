﻿using FolderCheck.Infrastructure.Extentions;
using FolderCheck.Integration.BioOptronics.Client.Intefaces;
using System.Security;

namespace FolderCheck.Integration.BioOptronics.Client
{
    public class BioOptronicsClientOptions : IBioOptronicsClientOptions
    {
        public string BaseUrl { get; }
        public int RetriesNumber { get; }
        public string AccessTokenKey { get; }

        public SecureString Username { get; }

        public SecureString Password { get; }

        public BioOptronicsClientOptions(string baseUrl, string accessTokenKey, int retriesNumber, string username, string password)
        {
            BaseUrl = baseUrl;
            AccessTokenKey = accessTokenKey;
            RetriesNumber = retriesNumber;

            Username = username.ToSecureString();
            Password = password.ToSecureString();
        }
    }
}
