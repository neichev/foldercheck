﻿using System.Security;

namespace FolderCheck.Integration.BioOptronics.Client.Intefaces
{
    public interface IBioOptronicsClientOptions
    {
        string AccessTokenKey { get; }
        string BaseUrl { get;  }
        int RetriesNumber { get; }
        SecureString Username { get; }
        SecureString Password { get; }
    }
}