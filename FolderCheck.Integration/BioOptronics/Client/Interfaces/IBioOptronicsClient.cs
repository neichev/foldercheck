﻿using FolderCheck.Api.Contracts;
using System.Threading.Tasks;

namespace FolderCheck.Integration.BioOptronics.Client.Intefaces
{
    public interface IBioOptronicsClient
    {
        void Dispose();
        Task<Folder> GetFolder();
        Task<string> GetTokenAsync();
    }
}