﻿using FolderCheck.Api.Contracts;
using FolderCheck.Infrastructure.Extentions;
using FolderCheck.Infrastructure.Storages.Interfaces;
using FolderCheck.Integration.BioOptronics.Client.Intefaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Linq;
using IJsonConverter = FolderCheck.Infrastructure.Json.Interfaces.IJsonConverter;

namespace FolderCheck.Integration.BioOptronics.Client
{
    public class BioOptronicsClient : IDisposable, IBioOptronicsClient
    {
        private readonly ILogger<BioOptronicsClient> _logger;
        private readonly HttpClient _httpClient;
        private readonly IBioOptronicsClientOptions _options;
        private readonly IJsonConverter _jsonConverter;
        private readonly ISecureInMemoryStorage _secureInMemoryStorage;

        public BioOptronicsClient(IBioOptronicsClientOptions options, ILogger<BioOptronicsClient> logger, IJsonConverter jsonConverter, ISecureInMemoryStorage secureInMemoryStorage)
        {
            _logger = logger;
            _httpClient = new HttpClient();
            _options = options;
            _jsonConverter = jsonConverter;
            _secureInMemoryStorage = secureInMemoryStorage;
        }

        public async Task<string> GetTokenAsync()
        {
            try
            {
                var creds = new Dictionary<string, string>();
                creds.Add("username", _options.Username.AsString());
                creds.Add("password", _options.Password.AsString());

                var response = await _httpClient.PostAsync($"{_options.BaseUrl}token",
                    new FormUrlEncodedContent(creds)
                    );

                var body = _jsonConverter.Deserialize<Dictionary<string, string>>(await response.Content.ReadAsStringAsync());
                var token = body[_options.AccessTokenKey];

                _secureInMemoryStorage.Set(_options.AccessTokenKey, token);

                return token;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        public async Task<Folder> GetFolder()
        {
            // possible token expiration check if store both dates
            await GetTokenAsync();

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _secureInMemoryStorage.GetAsString(_options.AccessTokenKey));

            try
            {
                // possible retries
                var response = await _httpClient.GetAsync($"{_options.BaseUrl}folders");
                
                var folder = _jsonConverter.Deserialize<Folder>(await response.Content.ReadAsStringAsync());

                return folder;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                throw;
            }
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }
    }
}
