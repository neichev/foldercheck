﻿using FolderCheck.Integration.BioOptronics.Client;
using FolderCheck.Integration.BioOptronics.Client.Intefaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FolderCheck.Integration
{
    public static class IntegrationModule
    {
        public static void RegisterIntegration(this IServiceCollection services, IConfiguration config)
        {
            services.AddTransient<IBioOptronicsClientOptions, BioOptronicsClientOptions>(sp => new BioOptronicsClientOptions(
                config.GetValue<string>("baseUri"),
                config.GetValue<string>("accessTokenKey"),
                0,
                config.GetValue<string>("username"),
                config.GetValue<string>("password")));
            services.AddTransient<IBioOptronicsClient, BioOptronicsClient>();
        }
    }
}
