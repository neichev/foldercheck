﻿using FolderCheck.Services.Dtos;
using System.IO;
using System.Threading.Tasks;

namespace FolderCheck.Services.Interfaces
{
    public interface IFolderService
    {
        void CompareFolders(DirectoryInfo local, FolderDto remote);
        DirectoryInfo GetLocalFolder();
        Task<FolderDto> GetRemoteFolder();
    }
}