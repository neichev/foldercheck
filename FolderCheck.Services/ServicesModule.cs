﻿using AutoMapper;
using FolderCheck.Domain.Mapping.Profiles;
using FolderCheck.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FolderCheck.Services
{
    public static class ServicesModule
    {
        public static void RegisterServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddAutoMapper(typeof(GeneralProfile));
            services.AddTransient<IFolderService, FolderService>();
        }
    }
}
