﻿using AutoMapper;
using FolderCheck.Domain.Models;
using FolderCheck.Domain.Trees;
using FolderCheck.Domain.Trees.Builders.Interfaces;
using FolderCheck.Domain.Trees.SourceProviders.Interfaces;
using FolderCheck.Integration.BioOptronics.Client.Intefaces;
using FolderCheck.Services.Dtos;
using FolderCheck.Services.Interfaces;
using System;
using System.IO;
using System.Threading.Tasks;

namespace FolderCheck.Services
{
    // just a wrapper on CLI logic so far
    public class FolderService : IFolderService
    {
        private readonly IMapper _mapper;
        private readonly IBioOptronicsClient _bioOptronicsClient;
        private readonly ISourceProvider<DirectoryInfo, FileSystemInfo> _localSP;
        private readonly ITreeBuilder<FSTree, ISourceProvider<DirectoryInfo, FileSystemInfo>> _localTB;
        private readonly ISourceProvider<Folder, Content> _remoteSP;
        private readonly ITreeBuilder<FSTree, ISourceProvider<Folder, Content>> _remoteTB;

        public FolderService(IMapper mapper, IBioOptronicsClient bioOptronicsClient,
            ISourceProvider<DirectoryInfo, FileSystemInfo> localSP,
            ITreeBuilder<FSTree, ISourceProvider<DirectoryInfo, FileSystemInfo>> localTB,
            ISourceProvider<Folder, Content> remoteSP,
            ITreeBuilder<FSTree, ISourceProvider<Folder, Content>> remoteTB)
        {
            _mapper = mapper;
            _bioOptronicsClient = bioOptronicsClient;
            _localSP = localSP;
            _localTB = localTB;
            _remoteSP = remoteSP;
            _remoteTB = remoteTB;
        }

        public DirectoryInfo GetLocalFolder()
        {
            // Prompt a local dir
            Console.WriteLine("Please paste a local folder path to compare:");
            var localPath = Console.ReadLine();
            // Validate the dir
            if (!Directory.Exists(localPath))
            {
                throw new DirectoryNotFoundException(localPath);
            }
            return new DirectoryInfo(localPath);
        }

        public async Task<FolderDto> GetRemoteFolder()
        {
            return _mapper.Map<FolderDto>(await _bioOptronicsClient.GetFolder());
        }

        public void CompareFolders(DirectoryInfo local, FolderDto remote)
        {
            _localSP.Initialize(() => local);

            var localTree = _localTB.Build();

            _remoteSP.Initialize(() => _mapper.Map<Folder>(remote));

            var remoteTree = _remoteTB.Build();

            localTree.CompareTo(remoteTree);
        }
    }
}
