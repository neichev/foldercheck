﻿using AutoMapper;
using System.Collections.Generic;

namespace FolderCheck.Services.Mapping.Converters
{
    public class FolderConverter : ITypeConverter<Api.Contracts.Folder, Services.Dtos.FolderDto>,
        ITypeConverter<Api.Contracts.AnyContent, Services.Dtos.FolderDto>,
        ITypeConverter<Services.Dtos.FolderDto, Domain.Models.Folder>
    {
        public Services.Dtos.FolderDto Convert(Api.Contracts.Folder source, Services.Dtos.FolderDto destination, ResolutionContext context)
        {
            destination = destination ?? new Services.Dtos.FolderDto();

            destination.Id = source.Id;
            destination.Name = source.Name;
            destination.CreatedOn = source.CreatedOn;
            destination.LastModified = source.LastModified;
            destination.Size = source.Size;
            destination.Items = new List<Services.Dtos.ContentDto>();

            foreach (var item in source.Items)
            {
                if (item.Items != null)
                {
                    destination.Items.Add(context.Mapper.Map<Api.Contracts.AnyContent, Services.Dtos.FolderDto>(item));
                    continue;
                }
                destination.Items.Add(context.Mapper.Map<Api.Contracts.AnyContent, Services.Dtos.FileDto>(item));
            }

            return destination;
        }

        public Services.Dtos.FolderDto Convert(Api.Contracts.AnyContent source, Services.Dtos.FolderDto destination, ResolutionContext context)
        {
            return Convert(context.Mapper.Map<Api.Contracts.AnyContent, Api.Contracts.Folder>(source), destination, context);
        }

        public Domain.Models.Folder Convert(Services.Dtos.FolderDto source, Domain.Models.Folder destination, ResolutionContext context)
        {
            destination = destination ?? new Domain.Models.Folder();

            destination.Id = source.Id;
            destination.Name = source.Name;
            destination.CreatedOn = source.CreatedOn;
            destination.LastModified = source.LastModified;
            destination.Size = source.Size;
            destination.Items = new List<Domain.Models.Content>();

            foreach (var item in source.Items)
            {
                if (item is Services.Dtos.FileDto)
                {
                    destination.Items.Add(context.Mapper.Map<Services.Dtos.FileDto, Domain.Models.File>((Services.Dtos.FileDto)item));
                    continue;
                }
                destination.Items.Add(context.Mapper.Map<Services.Dtos.FolderDto, Domain.Models.Folder>((Services.Dtos.FolderDto)item));
            }

            return destination;
        }
    }
}
