﻿using AutoMapper;
using FolderCheck.Domain.Models;
using System.IO;
using System.Linq;

namespace FolderCheck.Services.Mapping.Converters
{
    public class FileSystemInfoConverter : ITypeConverter<DirectoryInfo, Domain.Models.Folder>,
        ITypeConverter<FileSystemInfo, Domain.Models.File>,
        ITypeConverter<FileSystemInfo, Domain.Models.Content>,

        IValueConverter<DirectoryInfo, Domain.Models.Folder>,
        IValueConverter<FileSystemInfo, Domain.Models.Content>
    {
        public Folder Convert(DirectoryInfo source, ResolutionContext context)
        {
            Folder dest = (Folder)((IValueConverter<FileSystemInfo, Domain.Models.Content>)this).Convert(source, context);

            return dest;
        }

        public Folder Convert(DirectoryInfo source, Folder destination, ResolutionContext context)
        {
            return Convert(source, context);
        }

        public Content Convert(FileSystemInfo source, ResolutionContext context)
        {
            Content dest;

            if (source is DirectoryInfo)
            {
                dest = new Folder();
                Convert(source, dest, context);
                ((Folder)dest).Items = ((DirectoryInfo)source).GetFileSystemInfos().Select(fsi => Convert(fsi, context)).ToList();
            }
            else
            {
                dest = new Domain.Models.File();
                Convert(source, dest, context);
            }

            return dest;
        }

        public Content Convert(FileSystemInfo source, Content destination, ResolutionContext context)
        {
            destination = destination ?? new Content();

            destination.CreatedOn = source.CreationTimeUtc;
            destination.Id = source.GetHashCode(); // idk what to put here
            destination.LastModified = source.LastWriteTimeUtc;
            destination.Name = source.Name;
            destination.Size = 0; // skipped for now

            return destination;
        }

        public Domain.Models.File Convert(FileSystemInfo source, Domain.Models.File destination, ResolutionContext context)
        {
            return (Domain.Models.File)Convert(source, context);
        }
    }
}
