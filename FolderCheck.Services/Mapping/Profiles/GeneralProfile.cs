﻿using AutoMapper;
using FolderCheck.Domain.Models;
using FolderCheck.Services.Dtos;
using FolderCheck.Services.Mapping.Converters;

namespace FolderCheck.Domain.Mapping.Profiles
{
    // I didn't want to split it on several profiles
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            CreateMap<System.IO.DirectoryInfo, Folder>()
                .ConvertUsing<FileSystemInfoConverter>();

            CreateMap<System.IO.FileSystemInfo, File>()
                .ConvertUsing<FileSystemInfoConverter>();

            CreateMap<Api.Contracts.AnyContent, Api.Contracts.Folder>();

            CreateMap<Api.Contracts.Folder, FolderDto>()
                .ConvertUsing<FolderConverter>();

            CreateMap<Api.Contracts.AnyContent, FileDto>();

            CreateMap<Api.Contracts.AnyContent, FolderDto>()
                .ConvertUsing<FolderConverter>();

            CreateMap<FolderDto, Folder>()
                .ConvertUsing<FolderConverter>();
            CreateMap<FileDto, File>();
            CreateMap<ContentDto, Content>();

            CreateMap<AnyContent, Folder>();
            CreateMap<AnyContent, File>();
        }
    }
}
