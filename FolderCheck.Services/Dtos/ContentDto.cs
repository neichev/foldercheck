﻿using System;

namespace FolderCheck.Services.Dtos
{
    public class ContentDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime LastModified { get; set; }
        public DateTime CreatedOn { get; set; }
        public long Size { get; set; }
    }
}
