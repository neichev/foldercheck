﻿namespace FolderCheck.Services.Dtos
{
    public class FileDto : ContentDto
    {
        public string Data { get; set; }
    }
}
