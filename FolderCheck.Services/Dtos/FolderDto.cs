﻿using System.Collections.Generic;

namespace FolderCheck.Services.Dtos
{
    public class FolderDto : ContentDto
    {
        public IList<ContentDto> Items { get; set; }
    }
}
