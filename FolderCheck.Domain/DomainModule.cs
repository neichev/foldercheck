﻿using FolderCheck.Domain.Models;
using FolderCheck.Domain.Trees;
using FolderCheck.Domain.Trees.Builders;
using FolderCheck.Domain.Trees.Builders.Interfaces;
using FolderCheck.Domain.Trees.SourceProviders;
using FolderCheck.Domain.Trees.SourceProviders.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace FolderCheck.Domain
{
    public static class DomainModule
    {
        public static void RegisterDomain(this IServiceCollection services, IConfiguration config)
        {
            services.AddScoped<ISourceProvider<DirectoryInfo, FileSystemInfo>, FileSystemSourceProvider>();

            services.AddScoped<ISourceProvider<Folder, Content>, RemoteSourceProvider>();

            services.AddTransient(typeof(ITreeBuilder<FSTree, ISourceProvider<DirectoryInfo, FileSystemInfo>>),
                                    typeof(FSTreeBuilder<ISourceProvider<DirectoryInfo, FileSystemInfo>, DirectoryInfo, FileSystemInfo>));
            services.AddTransient(typeof(ITreeBuilder<FSTree, ISourceProvider<Folder, Content>>),
                                    typeof(FSTreeBuilder<ISourceProvider<Folder, Content>,Folder,Content>));
        }
    }
}
