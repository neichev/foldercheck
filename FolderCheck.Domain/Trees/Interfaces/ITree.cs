﻿using FolderCheck.Domain.Trees.Nodes;
using FolderCheck.Domain.Trees.Nodes.Interfaces;
using System;

namespace FolderCheck.Domain.Trees.Interfaces
{
    public interface ITree<TNode, TData> : IComparable<ITree<TNode, TData>>, IDisposable 
        where TNode : INode<TData> 
        where TData : IEquatable<TData>
    {
        TNode Root { get; }
        TNode Insert(TNode node, TData value);
    }
}
