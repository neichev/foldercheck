﻿using FolderCheck.Domain.Trees.Interfaces;
using FolderCheck.Domain.Trees.Nodes;
using System;

namespace FolderCheck.Domain.Trees
{
    public abstract class Tree<TNode, TData> : ITree<TNode, TData>
        where TNode : Node<TData>
        where TData : IEquatable<TData>
    {
        public TNode Root { get; private set; }

        public Tree(TNode root)
        {
            Root = root;
        }

        public abstract TNode Insert(TNode node, TData value);
        
        public abstract void Dispose();

        public virtual int CompareTo(ITree<TNode, TData> other)
        {
            return Root.CompareTo(other.Root);
        }
    }
}
