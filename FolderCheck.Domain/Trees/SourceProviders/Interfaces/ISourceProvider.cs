﻿using System;
using System.Collections.Generic;

namespace FolderCheck.Domain.Trees.SourceProviders.Interfaces
{
    public interface ISourceProvider<TContainer, TItem> where TContainer : TItem
    {
        TContainer Root { get; }
        Dictionary<string, object> MetaData { get; }
        TContainer Initialize(Func<TContainer> factory);
        IEnumerable<TItem> GetElements(TContainer current);
        bool IsLeaf(TItem item);
        bool IsNode(TItem item);
    }
}
