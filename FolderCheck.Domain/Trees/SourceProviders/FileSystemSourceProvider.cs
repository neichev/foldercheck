﻿using FolderCheck.Domain.Trees.SourceProviders.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;

namespace FolderCheck.Domain.Trees.SourceProviders
{
    public class FileSystemSourceProvider : ISourceProvider<DirectoryInfo, FileSystemInfo>
    {
        public DirectoryInfo Root { get; private set; }

        public Dictionary<string, object> MetaData { get; private set; }

        public FileSystemSourceProvider()
        {
            MetaData = new Dictionary<string, object>() 
            {
                { "Separator", "\\"}
            };
        }

        public DirectoryInfo Initialize(Func<DirectoryInfo> factory)
        {
            Root = factory();
            return Root;
        }

        public IEnumerable<FileSystemInfo> GetElements(DirectoryInfo current)
        {
            return current.GetFileSystemInfos("*");
        }

        public bool IsLeaf(FileSystemInfo item)
        {
            return item is FileInfo;
        }

        public bool IsNode(FileSystemInfo item)
        {
            return item is DirectoryInfo;
        }
    }
}
