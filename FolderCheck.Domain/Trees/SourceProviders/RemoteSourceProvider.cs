﻿using FolderCheck.Domain.Models;
using FolderCheck.Domain.Trees.SourceProviders.Interfaces;
using System;
using System.Collections.Generic;

namespace FolderCheck.Domain.Trees.SourceProviders
{
    public class RemoteSourceProvider: ISourceProvider<Folder, Content>
    {
        public Folder Root { get; private set; }

        public Dictionary<string, object> MetaData { get; private set; }

        public RemoteSourceProvider()
        {
            MetaData = new Dictionary<string, object>()
            {
                { "Separator", "\\"}
            };
        }

        public Folder Initialize(Func<Folder> factory)
        {
            Root = factory();
            return Root;
        }

        public IEnumerable<Content> GetElements(Folder current)
        {
            return current.Items;
        }

        public bool IsLeaf(Content item)
        {
            return item is File;
        }

        public bool IsNode(Content item)
        {
            return item is Folder;
        }
    }
}
