﻿using FolderCheck.Domain.Models;
using FolderCheck.Domain.Trees.Nodes.Interfaces;

namespace FolderCheck.Domain.Trees.Nodes
{
    public class FSNode : Node<Content>
    {
        private readonly string _separator;

        public FSNode(Node<Content> parent, Content data, string separator) : base(parent, data)
        {
            _separator = separator;
        }

        public override string ToString()
        {
            var str = $"{_separator}{Data.ToString()}";
            INode<Content> current = this;
            while (current.Parent != null)
            {
                str = $"{_separator}{current.Parent.Data?.ToString()}{str}";
                current = current.Parent;
            }
            return str;
        }
    }
}
