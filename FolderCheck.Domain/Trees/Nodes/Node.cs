﻿using FolderCheck.Domain.Trees.Nodes.Interfaces;
using System;
using System.Collections.Generic;

namespace FolderCheck.Domain.Trees.Nodes
{
    public abstract class Node<T> : Dictionary<string, Node<T>>, INode<T>, IComparable<Node<T>>, IDisposable/* C# doesn't support such covariance */ where T : IEquatable<T>
    {
        public T Data { get; private set; }
        public INode<T> Parent { get; private set; }

        public event Action<string, ComparisonResultEnum> EqualityCompared;

        public Node(Node<T> parent, T data)
        {
            Data = data;
            Parent = parent;
        }

        public abstract string ToString();

        public void Dispose()
        {
            var clients = EqualityCompared?.GetInvocationList();
            foreach (var handler in clients)
            {
                EqualityCompared -= (handler as Action<string, ComparisonResultEnum>);
            }

            foreach(var item in this.Values)
            {
                item.Dispose();
            }
        }

        public int CompareTo(Node<T> other)
        {
            if (!Data.Equals(other.Data))
            {
                EqualityCompared?.Invoke(this.ToString(), ComparisonResultEnum.NotEqual);
                return (int)ComparisonResultEnum.NotEqual;
            }

            foreach (var key in Keys)
            {
                var keyMatches = other.TryGetValue(key, out var their);
                if (!keyMatches)
                {
                    EqualityCompared?.Invoke(this[key].ToString(), ComparisonResultEnum.NotEqual);
                    continue;
                }

                var result = this[key].CompareTo(their);
                if (result != 0)
                {
                    EqualityCompared?.Invoke(this[key].ToString(), ComparisonResultEnum.NotEqual);
                    continue;
                }
                EqualityCompared?.Invoke(this[key].ToString(), ComparisonResultEnum.Equal);                
            }

            return 0;
        }

        // C# doesn't support such covariance
        // that's why a brute force cast is applied
        int IComparable<INode<T>>.CompareTo(INode<T> other)
        {
            return CompareTo((Node<T>)other as Node<T>);
        }
    }
}
