﻿using System;

namespace FolderCheck.Domain.Trees.Nodes.Interfaces
{
    public interface INode<T> : IComparable<INode<T>>, IDisposable where T : IEquatable<T>
    {
        INode<T> Parent { get; }
        T Data { get; }
        event Action<string, ComparisonResultEnum> EqualityCompared;
    }
}
