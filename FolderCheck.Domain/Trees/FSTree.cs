﻿using FolderCheck.Domain.Models;
using FolderCheck.Domain.Trees.Nodes;
using FolderCheck.Infrastructure.Extentions;
using System;

namespace FolderCheck.Domain.Trees
{
    public class FSTree : Tree<FSNode, Content>
    {
        public string Separator { get; private set; }

        public FSTree(FSNode root, string separator) : base(root)
        {
            Separator = separator;
            Root.EqualityCompared += OnNodeEqualityCompared;
        }

        public override FSNode Insert(FSNode node, Content value)
        {
            if (node == null)
            {
                throw new ArgumentNullException(nameof(node));
            }

            if (node.ContainsKey(value.Name))
            {
                throw new Exception("Such key already exists");
            }

            var newNode = new FSNode(node, value, Separator);

            newNode.EqualityCompared += OnNodeEqualityCompared;

            node.Add(value.Name, newNode);

            return newNode;
        }

        private void OnNodeEqualityCompared(string path, ComparisonResultEnum result)
        {
            if (result == ComparisonResultEnum.Equal)
                $"MATCHES IN BOTH: {path}".Green();
            if (result == ComparisonResultEnum.NotEqual)
                $"ABSENT IN TARGET: {path}".Red();
        }

        public override void Dispose()
        {
            Root.Dispose();
        }
    }
}
