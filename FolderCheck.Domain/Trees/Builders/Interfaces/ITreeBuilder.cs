﻿namespace FolderCheck.Domain.Trees.Builders.Interfaces
{
    public interface ITreeBuilder<TTree, out TSourceProvider>
    {
        TSourceProvider SourceProvider { get; }
        TTree Build();
    }
}
