﻿using AutoMapper;
using FolderCheck.Domain.Models;
using FolderCheck.Domain.Trees.Builders.Interfaces;
using FolderCheck.Domain.Trees.Nodes;
using FolderCheck.Domain.Trees.SourceProviders.Interfaces;
using System.Linq;
using File = FolderCheck.Domain.Models.File;

namespace FolderCheck.Domain.Trees.Builders
{
    public class FSTreeBuilder<TSourceProvider, TContainer, TItem> : ITreeBuilder<FSTree, TSourceProvider>
        where TSourceProvider : ISourceProvider<TContainer, TItem>
        where TContainer : TItem
    {
        private const string _separatorKey = "Separator";
        private readonly IMapper _mapper;

        public TSourceProvider SourceProvider { get; }

        public FSTreeBuilder(IMapper mapper, TSourceProvider sourceProvider)
        {
            _mapper = mapper;
            SourceProvider = sourceProvider;
        }

        public FSTree Build()
        {
            var rootNode = new FSNode(null, _mapper.Map<Folder>(SourceProvider.Root), (string)SourceProvider.MetaData[_separatorKey]);
            var tree = new FSTree(rootNode, (string)SourceProvider.MetaData[_separatorKey]);

            BuildTree(tree, rootNode, SourceProvider.Root);

            return tree;
        }

        private void BuildTree(FSTree tree, FSNode current, TContainer root)
        {
            var infos = SourceProvider.GetElements(root);

            var leafs = infos.Where(SourceProvider.IsLeaf).ToList();
            leafs.ForEach(leaf => tree.Insert(current, _mapper.Map<File>(leaf)));

            var nodes = infos.Where(SourceProvider.IsNode).ToList();
            foreach (var dir in nodes)
            {
                var addedNode = tree.Insert(current, _mapper.Map<Folder>(dir));
                BuildTree(tree, addedNode, (TContainer)dir);
            }
        }
    }
}
