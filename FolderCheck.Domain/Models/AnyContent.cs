﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FolderCheck.Domain.Models
{
    public class AnyContent : Content
    {
        public IList<AnyContent> Items { get; set; }
        public string Data { get; set; }
    }
}
