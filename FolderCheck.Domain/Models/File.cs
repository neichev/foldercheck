﻿using System;

namespace FolderCheck.Domain.Models
{
    public class File : Content, IEquatable<File>
    {
        public string Data { get; set; }

        public bool Equals(File other) => base.Equals(other);
    }
}
