﻿using System;

namespace FolderCheck.Domain.Models
{
    public class Content : IEquatable<Content>
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime LastModified { get; set; }
        public DateTime CreatedOn { get; set; }
        public long Size { get; set; }

        public bool Equals(Content other)
        {
            //if (Id != other.Id) return false;

            if (Name != other.Name) return false;
            //if (LastModified != other.LastModified) return false;
            //if (CreatedOn != other.CreatedOn) return false;

            //if (Size != other.Size) return false;

            return true;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
