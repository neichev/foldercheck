﻿using System;
using System.Collections.Generic;

namespace FolderCheck.Domain.Models
{
    public class Folder : Content, IEquatable<Folder>
    {
        public IList<Content> Items { get; set; }

        public bool Equals(Folder other) => base.Equals(other);
    }
}
