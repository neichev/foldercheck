﻿using FolderCheck.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace FolderCheck
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(() => MainAsync()).GetAwaiter().GetResult();

            Console.WriteLine("Done!");
            Console.ReadKey();
        }

        static async Task MainAsync()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

            var serviceProvider = IoC.Build(config);

            var folderService = serviceProvider.GetService<IFolderService>();

            var local = folderService.GetLocalFolder();

            var remote = await folderService.GetRemoteFolder();

            folderService.CompareFolders(local, remote);
        }
    }
}
