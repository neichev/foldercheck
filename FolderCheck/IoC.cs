﻿using FolderCheck.Domain;
using FolderCheck.Infrastructure;
using FolderCheck.Integration;
using FolderCheck.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FolderCheck
{
    internal class IoC
    {
        public static ServiceProvider Build(IConfiguration config)
        {
            var services = new ServiceCollection();

            services.RegisterInfrastructure(config);
            services.RegisterIntegration(config);
            services.RegisterDomain(config);
            services.RegisterServices(config);

            return services.BuildServiceProvider();
        }
    }
}
