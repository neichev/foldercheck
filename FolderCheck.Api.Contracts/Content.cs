﻿using System;

namespace FolderCheck.Api.Contracts
{
    public class Content
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime LastModified { get; set; }
        public DateTime CreatedOn { get; set; }
        public long Size { get; set; }
    }
}
