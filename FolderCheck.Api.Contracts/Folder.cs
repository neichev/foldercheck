﻿using System.Collections.Generic;

namespace FolderCheck.Api.Contracts
{
    public class Folder : Content
    {
        public IEnumerable<AnyContent> Items { get; set; }
    }
}
