﻿namespace FolderCheck.Api.Contracts
{
    public class File : Content
    {
        public string Data { get; set; }
    }
}
