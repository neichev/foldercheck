﻿using System.Collections.Generic;

namespace FolderCheck.Api.Contracts
{
    public class AnyContent: Content
    {
        public IEnumerable<AnyContent> Items { get; set; }
        public string Data { get; set; }
    }
}
