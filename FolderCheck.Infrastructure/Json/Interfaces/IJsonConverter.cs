﻿namespace FolderCheck.Infrastructure.Json.Interfaces
{
    public interface IJsonConverter
    {
        T Deserialize<T>(string json);
        string Serialize(object obj);
    }
}