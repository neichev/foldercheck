﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using IJsonConverter = FolderCheck.Infrastructure.Json.Interfaces.IJsonConverter;

namespace FolderCheck.Infrastructure.Json
{
    public class JsonConverter : IJsonConverter
    {
        private readonly ILogger<JsonConverter> _logger;
        private readonly JsonSerializerSettings _settings;

        public JsonConverter(ILogger<JsonConverter> logger)
        {
            _logger = logger;
            DefaultContractResolver contractResolver = new DefaultContractResolver
            {
                NamingStrategy = new CamelCaseNamingStrategy()
            };
            _settings = new JsonSerializerSettings
            {
                ContractResolver = contractResolver,
                Formatting = Formatting.Indented
            };
        }

        public string Serialize(object obj)
        {
            try
            {
                string json = JsonConvert.SerializeObject(obj, _settings);

                return json;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Serialization failed");
                throw;
            }
        }

        public T Deserialize<T>(string json)
        {
            try
            {
                T obj = JsonConvert.DeserializeObject<T>(json, _settings);

                return obj;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Deserialization failed");
                throw;
            }
        }
    }
}
