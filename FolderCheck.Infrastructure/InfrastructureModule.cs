﻿using FolderCheck.Infrastructure.Json;
using FolderCheck.Infrastructure.Json.Interfaces;
using FolderCheck.Infrastructure.Storages;
using FolderCheck.Infrastructure.Storages.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;

namespace FolderCheck.Infrastructure
{
    public static class InfrastructureModule
    {
        public static void RegisterInfrastructure(this IServiceCollection services, IConfiguration config)
        {

            services.AddTransient<IJsonConverter, JsonConverter>();
            services.AddSingleton<ISecureInMemoryStorage, SecureInMemoryStorage>();
            
            services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.ClearProviders();
                loggingBuilder.SetMinimumLevel(LogLevel.Trace);
                loggingBuilder.AddSerilog(new LoggerConfiguration().WriteTo.Console().CreateLogger());
            });
        }
    }
}
