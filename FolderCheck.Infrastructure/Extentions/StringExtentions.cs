﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;

namespace FolderCheck.Infrastructure.Extentions
{
    public static class StringExtentions
    {
        public static SecureString ToSecureString(this string input)
        {
            var ss = new SecureString();
            input.ToCharArray().ToList().ForEach(c => ss.AppendChar(c));
            return ss;
        }

        public static string AsString(this SecureString input)
        {
            IntPtr valuePtr = IntPtr.Zero;
            try
            {
                valuePtr = Marshal.SecureStringToGlobalAllocUnicode(input);
                return Marshal.PtrToStringUni(valuePtr);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(valuePtr);
            }
        }
    }
}
