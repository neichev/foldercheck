﻿using System;

namespace FolderCheck.Infrastructure.Extentions
{
    public static class ConsoleColoring
    {
        public static void Green(this string input)
        {
            input.Colored(ConsoleColor.Green);
        }

        public static void Red(this string input)
        {
            input.Colored(ConsoleColor.Red);
        }

        public static void White(this string input)
        {
            input.Colored(ConsoleColor.White);
        }

        public static void Colored(this string input, ConsoleColor color)
        {
            var prev = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.WriteLine(input);
            Console.ForegroundColor = prev;
        }
    }
}
