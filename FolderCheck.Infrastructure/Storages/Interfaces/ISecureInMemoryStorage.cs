﻿using System.Security;

namespace FolderCheck.Infrastructure.Storages.Interfaces
{
    public interface ISecureInMemoryStorage
    {
        void Add(string key, string value);
        void Set(string key, string value);
        void Dispose();
        SecureString Get(string key);
        string GetAsString(string key);
    }
}