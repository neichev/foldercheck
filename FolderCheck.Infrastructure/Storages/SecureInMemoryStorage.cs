﻿using FolderCheck.Infrastructure.Extentions;
using FolderCheck.Infrastructure.Storages.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;

namespace FolderCheck.Infrastructure.Storages
{
    public class SecureInMemoryStorage : IDisposable, ISecureInMemoryStorage
    {
        // each value stored in a single slice of memory, encrypted on Windows
        private readonly Dictionary<string, SecureString> _storage = new Dictionary<string, SecureString>();

        public void Add(string key, string value)
        {
            if (_storage.ContainsKey(key))
            {
                throw new Exception($"Key {key} already exists");
            }
            Set(key, value);
        }

        public void Set(string key, string value)
        {
            _storage[key] = value.ToSecureString();
        }

        public SecureString Get(string key)
        {
            if (_storage[key].IsReadOnly())
                return _storage[key];

            _storage[key].MakeReadOnly();
            return _storage[key];
        }

        public string GetAsString(string key)
        {
            if (!_storage[key].IsReadOnly())
                _storage[key].MakeReadOnly();

            return _storage[key].AsString();
        }

        public void Dispose()
        {
            foreach (var value in _storage.Values)
            {
                value.Clear();
                value.Dispose();
            }
            _storage.Clear();
        }
    }
}
